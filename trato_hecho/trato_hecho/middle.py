from django.contrib.sites.models import Site

class SiteMiddleware(object):
    def process_request(self, request):
        site = Site.objects.get_current()
        request.site = site
