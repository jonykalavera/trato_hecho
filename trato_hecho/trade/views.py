from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from fandjango.decorators import facebook_authorization_required
from django.http import Http404
from .models import TradeItem, TradeOffer

from trade.forms import TradeItemForm, TradeOfferForm
from trade.models import TradeItem, TradeOffer

@facebook_authorization_required
def index(request):
    objects = TradeItem.objects.exclude(owner=request.facebook.user)
    context = {
        'tradeitems': objects,
        'active':'index',
    }
    return render(request, 'tradeitem/list.html', context)
@facebook_authorization_required
def items(request):
    objects = request.facebook.user.tradeitem_set.order_by('-status', '-updated_at')
    context = {
        'tradeitems': objects,
        'active':'items',
    }
    return render(request, 'tradeitem/list.html', context)

@facebook_authorization_required
def offers(request):
    offers = TradeOffer.objects.filter(offered__owner=request.facebook.user,
        status='snt')
    context = {
        'offers':offers,
        'active':'offers',
    }
    return render(request, 'tradeitem/offers.html', context)

@facebook_authorization_required
def add_item(request):
    form = TradeItemForm()
    if request.method == "POST":
        form = TradeItemForm(request.POST, request.FILES)
        if form.is_valid():
            item = form.save(commit=False)
            item.owner = request.facebook.user
            item.save()
            # Without this next line the tags won't be saved.
            form.save_m2m()
            item.trade_to_fb(request)
            messages.success(request, "Published a new item successfuly.")
            return redirect(reverse('item_detail',args=[item.pk]))
    context = {
        'form':form,
        'active':'items',
    }
    return render(request, 'tradeitem/add_item.html', context)

@facebook_authorization_required
def item_detail(request, item_id):
    item = get_object_or_404(TradeItem, pk=item_id)
    related_offers =TradeOffer.objects.filter(status='apr').filter(
        Q(target=item)|Q(offered=item)).distinct()
    #if related_offers:
    #    return redirect(reverse('offer_detail', args=[related_offers[0].pk]))
    suggestions , is_owner = [], False
    template = 'tradeitem/item_detail.html'
    if item.owner == request.facebook.user:
        is_owner=True
        search_terms = item.tradeable_for.values_list('name', flat=True)
        if search_terms:
            suggestions = TradeItem.objects.filter(
                tags__name__in=search_terms).distinct()
        template = 'tradeitem/item_detail_owner.html'
    context = {
        'item':item,
        'is_owner':is_owner,
        'suggestions': suggestions,
        'active':'items',
    }
    return render(request, template, context)

@facebook_authorization_required
def make_offer(request, item_id):
    item = get_object_or_404(TradeItem, pk=item_id, status='pub')
    offer_form = TradeOfferForm()
    user_items = TradeItem.objects.filter(status='pub',
        owner=request.facebook.user).exclude(offered_for__target=item)
    offer_form.fields['offered'].queryset = user_items
    item_form = TradeItemForm()
    if request.method == "POST":
        offered_id = None
        if request.POST.get('action')=='from_new':
            item_form = TradeItemForm(request.POST, request.FILES)
            if item_form.is_valid():
                #save new offered item
                offered = item_form.save(commit=False)
                offered.owner=request.facebook.user
                offered.save()
                item_form.save_m2m()
                offered_id = offered.pk
        else:
            offered_id = request.POST.get('offered')
        if offered_id:
            offer_form = TradeOfferForm({
                'offered':offered_id,
            })
            offer_form.fields['offered'].queryset = user_items
            if offer_form.is_valid():
                offer = offer_form.save(commit=False)
                offer.target = item
                offer.save()
                messages.success(request, "Offer successfuly sent to the Item's owner")
                return redirect(reverse('offers'))
    context = {
        'offer_form':offer_form,
        'item_form':item_form,
        'item':item,
        'user_items':user_items,
        'active':'offers',
    }
    return render(request, 'tradeitem/make_offer.html', context)


@facebook_authorization_required
def offer_detail(request, offer_id):
    offer = get_object_or_404(TradeOffer, pk=offer_id, status='apr')
    template = 'tradeitem/offer_detail.html'
    context = {
        'offer':item,
        'active':'offers',
    }
    return render(request, "tradeitem/offer_detail.html", context)


@facebook_authorization_required
def accept_offer(request, offer_id):
    offer = get_object_or_404(TradeOffer, pk=offer_id)
      #, status='snt', target__owner=request.facebook.user)
    offer.accept(request)
    return redirect(reverse('items'))

@facebook_authorization_required
def withdraw_offer(request, offer_id):
    if not request.method=="POST":
        raise Http404
    try:
        offer = TradeOffer.objects.get(pk=offer_id, status='snt',
        offered__owner=request.facebook.user)
        offer.withdraw(request)
    except:
        messages.error(request, "Failed to withdraw the offer.")
    return redirect(reverse('offers'))
