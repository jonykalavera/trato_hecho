import os
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from django.core.urlresolvers import reverse
from fandjango.models import User
from taggit.managers import TaggableManager
from taggit.models import GenericTaggedItemBase, TaggedItemBase

def item_picture_path(instance, filename):
        import hashlib
        import random
        extension = filename.split('.')[-1]
        name = hashlib.md5(filename.encode('utf-8')+str(random.random())).hexdigest()
        return os.path.join("uploads","items",
            "%s.%s" % (name,extension))

class DatedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract=True

class SearchTermsManager(TaggableManager):
    def __init__(self, verbose_name=_("Search Terms"),
        help_text=_("A comma-separated list of search terms."),
        through=None, blank=False, related_name=None):
        super(SearchTermsManager, self).__init__(verbose_name,
            help_text, through, blank)
        self.rel.related_name=related_name


class SearchTerm(GenericTaggedItemBase, TaggedItemBase):
    class Meta:
        verbose_name = _("Search Term")
        verbose_name_plural = _("Search Terms")

ITEM_STATUS = (
    ('pub','published'),
    ('trd', 'traded',),
    ('end', 'ended'),
)

class TradeItem(DatedModel):
    owner = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    picture = models.ImageField(upload_to=item_picture_path)
    description = models.TextField()
    tags = TaggableManager()
    tradeable_for = SearchTermsManager(blank=True, through=SearchTerm,
        related_name='matching_set')
    status = models.CharField(max_length=3, choices=ITEM_STATUS,
        default='pub')

    def __unicode__(self):
        return unicode(self.name)

    @models.permalink
    def get_absolute_url(self):
        return ("item_detail", [str(self.id)])
    @property
    def get_picture(self):
        picture = self.picture
        if picture:
            return picture
        return "http://placehold.it/140x140"

    def trade_to_fb(self, request):
        from facepy.graph_api import GraphAPI
        from django.contrib.sites.models import Site
        from django.conf import settings
        token = request.facebook.user.oauth_token.token
        graph = GraphAPI(oauth_token = token)
        domain = Site.objects.get_current()
        url = "http://%s%s" %( domain.domain,self.get_absolute_url())
        method = "me/%s:offer" % settings.FACEBOOK_APPLICATION_NAMESPACE
        try:
	    graph.post(path=method, item=url)
        except:
            pass

    def get_active_offers(self):
        return self.offered_trades.filter(status='snt').order_by('-created_at')

    def get_active_proposals(self):
        return self.offered_for.filter(status='snt').order_by('-created_at')


OFFER_STATUS=(
    ('snt', 'sent'),
    ('rjt', 'rejected'),
    ('wdn', 'withdrawn'),
    ('apr', 'aprooved'),
)

class TradeOffer(DatedModel):
    target = models.ForeignKey(TradeItem, related_name='offered_trades')
    offered = models.ForeignKey(TradeItem, related_name='offered_for')
    status = models.CharField(max_length=3, choices=OFFER_STATUS, default='snt')

    @models.permalink
    def get_absolute_url(self):
        return ("offers" ,)

    def accept(self, request):
        self.status = 'apr'
        self.save()
        obj = "%s - %s" % (self.target, self.offered)
        messages.success(request, "Done deal! you have accepted %s's offer." % obj)
        from facepy.graph_api import GraphAPI
        from django.contrib.sites.models import Site
        from django.conf import settings
        token = request.facebook.user.oauth_token.token
        graph = GraphAPI(oauth_token = token)
        domain = Site.objects.get_current()
        url = "http://%s%s" %( domain.domain,self.get_absolute_url())
        method = "me/%s:trade" % settings.FACEBOOK_APPLICATION_NAMESPACE
        try:
            graph.post(path=method, item=url)
        except:
            pass


    def withdraw(self, request):
        self.status = 'wdn'
        self.save()
        messages.success(request, "Your offer for %s has been withdrawn." % self.target.name)

    def get_absolute_url(self):
        return reverse('offer_detail', args=[self.pk])
