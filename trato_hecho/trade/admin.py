from django.contrib import admin

from trade.models import TradeItem, TradeOffer

class TradeItemAdmin(admin.ModelAdmin):
    list_display= ('name', 'owner',)

class TradeOfferAdmin(admin.ModelAdmin):
    list_display = ("target","offered","status")

admin.site.register(TradeItem, TradeItemAdmin)

admin.site.register(TradeOffer, TradeOfferAdmin)
