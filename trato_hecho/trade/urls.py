from django.conf.urls import patterns, include, url

urlpatterns = patterns('trade.views',
    url(r'^items/$', 'items', name='items'),
    url(r'^items/add/$', 'add_item', name='add_item'),
    url(r'^items/(?P<item_id>\d+)/$', 'item_detail', name='item_detail'),
    url(r'^items/(?P<item_id>\d+)/offer/$', 'make_offer', name='make_offer'),
    url(r'^offers/$', 'offers', name='offers'),
    url(r'^offers/$', 'offers', name='offers'),
    url(r'^offers/(?P<item_id>\d+)/$', 'offer_detail', name='offer_detail'),
    url(r'^offers/(?P<offer_id>\d+)/accept$', 'accept_offer', name='accept_offer'),
    url(r'^offers/(?P<offer_id>\d+)/withdraw$', 'withdraw_offer', name='withdraw_offer'),
    url(r'^$', 'index', name='index'),
)
